#!/bin/bash

# Console Colors
#=============================================================================
NC='\033[0m'

RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'

LRED='\033[01;31m'
LGREEN='\033[01;32m'
LYELLOW='\033[01;33m'
LBLUE='\033[01;34m'
LPURPLE='\033[01;35m'
LCYAN='\033[01;36m'
WHITE='\033[01;37m'
#=============================================================================

function main() {
	
	check_args "$@"
	check_dirs "$@"

    test_rsync "$@"
    confirm_execute_rsync "$@"
    execute_rsync "$@"
}

function check_args() {
    
    if [[ "$#" -lt 2 ]]; then
		echo -e "${RED}Error: this script must be run with at least two arguments.${NC}"
		echo ""
		print_help
		exit 1
    fi
}

function print_help() {

    echo -e "${LBLUE}Usage: "$0" [{path source 1}, {path source 2}, ... ] {path backup location}${NC}"
}

function check_dirs() {

    local arg_array=("$@")
	for i in "${arg_array[@]}"
	do
		check_dir_exists $i
	done
}

function check_dir_exists() {

	if [[ ! -d "$1" ]]; then
        echo -e "${RED}$1 does not exist.${NC}"
        exit 1
    fi
}

function test_rsync() {

	echo "Dry-run of rsync routines:"

	local src_array=("$@")
	local trgt_root=${src_array[-1]}
	unset src_array[-1]

	local i=1
	for src in "${src_array[@]}"
	do
		local trgt="$trgt_root/$(basename $src)"
		local rsync_src="$src/"
		local rsync_trgt="$trgt/"

		if [[ ! -d "$trgt" ]]; then
			echo "${i}: $src --> $trgt (new dir)"
			echo ""
		else
			echo "${i}: $src --> $trgt"
			echo "---------------------------"
			echo "Files that will be deleted:"
			echo -e "---------------------------${RED}"
			rsync -avn --delete $rsync_src $rsync_trgt | grep "deleting"
			echo -e "${NC}"
		fi
		i=$(($i + 1))
	done
}

function confirm_execute_rsync() {

	echo -e "${RED}Warning you are about to execute rsync for following locations"'!'":${NC}"

	local src_array=("$@")
	local trgt_root=${src_array[-1]}
	unset src_array[-1]

	local i=1
	for src in "${src_array[@]}"
	do
		local trgt="$trgt_root/$(basename $src)"
		local rsync_src="$src/"
		local rsync_trgt="$trgt/"

		if [[ ! -d "$trgt" ]]; then
			echo "${i}: $src --> $trgt (new dir)"
		else
			echo "${i}: $src --> $trgt"
		fi
		i=$(($i + 1))
	done

	echo ""
	read -p "Do you want to continue? [y/N] " yn
	case $yn in
		[Yy]* )
			;;
		[Nn]* )
			exit
			;;
		* )
			exit
			;;
	esac
	echo ""
}

function execute_rsync() {

	echo "Executing rsync routines:"

	local src_array=("$@")
	local trgt_root=${src_array[-1]}
	unset src_array[-1]

	local i=1
	for src in "${src_array[@]}"
	do
		local trgt="$trgt_root/$(basename $src)"
		local rsync_src="$src/"
		local rsync_trgt="$trgt/"

		if [[ ! -d "$trgt" ]]; then
			echo -e "${i}: ${GREEN}$src --> $trgt (new dir)${NC}"
			mkdir -p $trgt_dir
		else
			echo -e "${i}: ${GREEN}$src --> $trgt${NC}"
		fi

		rsync -avhh --delete --info=progress2 $rsync_src $rsync_trgt
		i=$(($i + 1))
	done
}

main "$@"
